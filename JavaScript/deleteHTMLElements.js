// Getting collection of elements by their class names.
const vegetables = document.getElementsByClassName("vegetables")
// Getting a collection of elements by their tag names.
const ul = document.getElementsByTagName("ul")
// Getting the 3rd item from the collection.
const element = vegetables[2]
// Removing a list item from the ul element.
ul[0].removeChild(element)
// Creating a new list item.
const li = document.createElement("li")
// Assigning a text to the list item.
li.innerText = "Apples"
// Setting a value for the class attribute vegetables.
li.setAttribute("class", "vegetables")
// Replacing the first item with the newly created list item.
ul[0].replaceChild(li, vegetables[0])