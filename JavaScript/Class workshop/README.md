# Saturday 8 oct 2022 Workshop on JavaScript.

* Build an application that enables the customer to carry out a transaction i.e., actually pays for an item.
## what will be needed
* The cost of the item.
* The currency of the price.
### Customer
  * Customer name.
  * Customer Id.
  * Card Id.
  * Date of birth.
  * Address.
#### What can the customer do?
  * Pay for an item.
  * Add item to cart.
  * Search for an item.
### Card
  * The type (visa or MasterCard).
  * The expiry date.
  * The security number.
  * The card user name.
  * Bank account number.
#### What can the card do?
  * Can issue a payment request to the bank.
  * Get the account balance.

# Satruday 15/10/2022

## Task three (TaskThree.html)
* Track the motion of the mouse (x, y) coordinates on an HTML element, and equally the size of the screen.

