/**
 * 
 * @param {type} typeOfCard 
 * @param {Date} expiryDate 
 * @param {string} securityNum 
 * @param {string} cardUserName 
 * @param {string} accountNum 
 * @param {function} issuePaymentRequest 
 */
function Card(typeOfCard, expiryDate, securityNum, cardUserName,
     accountNum, issuePaymentRequest){
    this.type = typeOfCard;
    this.expiryDate = expiryDate;
    this.securityNum = securityNum;
    this.cardUserName = cardUserName;
    this.accountNum = accountNum;
    this.issuePaymentRequest = issuePaymentRequest;
    this.getAccountBalance = issuePaymentRequest(this.securityNum, 
        this.accountNum, 700);
}
const type  = {
    VISA: "Visa",
    MASTERCARD: "MasterCard",
    AMEX: "American Express"
}