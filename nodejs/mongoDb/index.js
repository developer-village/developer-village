require('dotenv').config();
const MongoClient = require('mongodb').MongoClient;
const url = `mongodb+srv://${process.env.MONGOUSER}:${process.env.PASSWORD}@cluster0.upsk9.mongodb.net/newDB`;
const Mongo = require('./mongo/mongo.js')

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  // Mongo.findOne(db, "mydb", "Users", {"firstName":"Bruce"})
  // Mongo.createCollection(db, "newDB", "test") 
  // Mongo.insertIntoCollection(db, "newDB",{"firstName":"Manoh", "lastName":"test", "email":"Manoh@gmail.com"}, "test")
  // Mongo.findAll(db, "mydb", "Users", {}, {"firstName": 1}, {"firstName": -1})
  // Mongo.findSome(db, "mydb", "Users", {"lastName":"Wayne"})
  // Mongo.query(db, "mydb", {"lastName":"Wayne"}, "Users")
  // Mongo.deleteOne(db, "mydb", {"firstName":"Barack"}, "Students")
  // Mongo.deleteMany(db, "mydb", {"firstName": /^Tony$/}, "Users")
  // Mongo.dropCollection(db, "mydb", "Students")
});