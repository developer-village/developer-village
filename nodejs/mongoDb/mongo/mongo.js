function createCollection(db, nameOfDb, nameOfCollection){
    const dbo = db.db(nameOfDb);
    dbo.createCollection(nameOfCollection, function(err, res){
        if (err) throw err;
        console.log("Collection created!")
        db.close();
    });
}

function insertIntoCollection(db, nameOfDb, objects = {}, nameOfCollection){
    const dbo = db.db(nameOfDb);
    const myobj = objects;
    dbo.collection(nameOfCollection).insertOne(myobj, function(err, res){
        if (err) throw err;
        console.log("1 document inserted!")
        db.close();
    });
}

function findOne(db, nameOfDb, nameOfCollection, options = {}){
  const dbo = db.db(nameOfDb);
  dbo.collection(nameOfCollection).findOne(options, function(err, result) {
    if (err) throw err;
    console.log(result);
    db.close();
  });
}


function findSome(db, nameOfDb, nameOfCollection, options = {}){
  const dbo = db.db(nameOfDb);
  dbo.collection(nameOfCollection).find({}, {projection: options}).toArray(function(err, result) {
    if (err) throw err;
    console.log(result);
    db.close();
  });
}

function findAll(db, nameOfDb, nameOfCollection, options = {}, projections = {}, sort = {}){
  const dbo = db.db(nameOfDb);
  dbo.collection(nameOfCollection).find(options, {projection: projections}).sort(sort).
toArray(function(err, result) {
    if (err) throw err;
    console.log(result);
    db.close();
  });
}

function query(db, nameOfDb, queries = {}, nameOfCollection){
  const dbo = db.db(nameOfDb);
  const querys = queries;
  dbo.collection(nameOfCollection).find(querys).toArray(function(err, result){
    if (err) throw err;
    console.log(result);
    db.close();
  })
}

function deleteOne(db, nameOfDb, queryObject = {}, nameOfCollection ){
  const dbo = db.db(nameOfDb);
  const myquery = queryObject; 
  dbo.collection(nameOfCollection).deleteOne(myquery,
    (err, obj) => {
      if (err) throw err;
      console.log(obj);
      db.close();
    });
}

function deleteMany(db, nameOfDb, queryObject = {}, nameOfCollection ){
    const dbo = db.db(nameOfDb);
    const myquery = queryObject; 
    dbo.collection(nameOfCollection).deleteMany(myquery,
      (err, obj) => {
        if (err) throw err;
        // console.log(obj);
        console.log(obj.deletedCount + " object(s) were deleted.");
        db.close();
      });
    }

function dropCollection(db, nameOfDb, nameOfCollection){
    const dbo = db.db(nameOfDb);
    dbo.collection(nameOfCollection).drop(function(err, delOk) {
        if (err) throw err;
        if (delOk) console.log("collection deleted!")
        db.close();
    })
}

const Mongo = function(){}

Mongo.prototype.createCollection = createCollection
Mongo.prototype.insertIntoCollection = insertIntoCollection
Mongo.prototype.findOne = findOne
Mongo.prototype.findSome = findSome
Mongo.prototype.findAll = findAll
Mongo.prototype.query = query
Mongo.prototype.deleteOne = deleteOne
Mongo.prototype.deleteMany = deleteMany
Mongo.prototype.dropCollection = dropCollection


module.exports = new Mongo()