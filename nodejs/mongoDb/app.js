const MongoClient = require('mongodb').MongoClient
require('dotenv').config()
const url = `mongodb+srv://${process.env.MONGOUSER}:${process.env.PASSWORD}@cluster0.upsk9.mongodb.net/newDB`;


MongoClient.connect(url, function(err, db){
    if(err) throw err;
    console.log('connection succesful.')
    db.close()
})