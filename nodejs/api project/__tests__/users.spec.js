const users = require('../users.js')
describe("Users module", () => {
    test('it should have 4 users', () => {
        const numberOfUsers = users.getUsers().length
        expect(numberOfUsers).toBe(4)
    })
    test('it should get a single user', () => {
        // Arrange 
        const userList = users.getUsers()
        const numberOfUsers = userList.length
        const userId = 2
        const user = users.getSingleUser(userId)
        expect(userId).toBeLessThanOrEqual(numberOfUsers)
        expect(user.length).toBe(1)
        expect(userList.find(usr => usr.id == 2)).toBe(user[0])
    })
})