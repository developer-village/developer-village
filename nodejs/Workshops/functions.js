
/**
 * Arrow Functions
 */
const add = (a, b) => a + b;

function sub(a, b) {
  return a - b
}

const mul = (a, b) => {return a * b}

function div(a, b) {
  return a / b
}

function handler(func, a, b){
    return func(a,b)
}

handler(add, 5, 4)
