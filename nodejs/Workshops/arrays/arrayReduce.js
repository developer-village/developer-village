// use reduce to generate a greeting mssg for client.
// Require name , age and gender.

const clients = [
    {
        name: 'John',
        age: 36,
        gender: 'male'
    },
    {
        name: 'Mary',
        age: 26,
        gender: 'female'
    }
]

// creating a mssges variable which holds an empty array.
const messages = []
/**
 * 1. using "map" to loop through the clients array.
 * 2. using "object.values", to get all the values of individual objects in the client's array as an array.
 * 3. using "reduce" to concatenate the values in the individual clients' array objects.
 * 4. pushing the concatenated values of the individual clients objects from the clients' array into the the mssges' array.
 */
clients.map(x => messages.push(Object.values(x).reduce(function(total, currentVal){
    return total + " " + currentVal
})))
// looping through mssges and displying the concatenated messages from the clients' values array.
messages.forEach(message => console.log(message))