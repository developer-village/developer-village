const persons = [
    {
        firstName: 'John',
        lastName: 'Smith',
        age: 21,
        gender: 'male'
    },
    {
        firstName: 'Manoh',
        lastName: 'Sarah',
        age: 20,
        gender: 'female'
    },
    {
        firstName: 'Marc',
        lastName: 'Angelo',
        age: 17,
        gender: 'male'
    },
    {
        firstName: 'Halsey',
        lastName: 'SkinOfBlackPikin',
        age: 22,
        gender: 'male'
    }
];

function changeLastName(person){
    person.lastName = 'freddy'
}

persons.map(changeLastName)
// persons.map(item => item.lastName = 'freddy')

console.log(persons)