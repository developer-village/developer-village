const persons = [
    {
        firstName: 'John',
        lastName: 'Smith',
        age: 21,
        gender: gender.male
    },
    {
        firstName: 'Manoh',
        lastName: 'Sarah',
        age: 20,
        gender: gender.female
    },
    {
        firstName: 'Marc',
        lastName: 'Angelo',
        age: 17,
        gender: gender.male
    },
    {
        firstName: 'Halsey',
        lastName: 'SkinOfBlackPikin',
        age: 22,
        gender: gender.male
    }
];

const females = persons.filter(person => person.gender === 'female')
console.log(females)
const gender = {male: 'male', female: 'female'}