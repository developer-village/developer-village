const persons = [
    {
        name: 'John',
        age: 21,
        class: 'LSS'
    },
    {
        name: 'Mary',
        age: 19,
        class: 'LSA'
    },
    {
        name: 'Mike',
        age: 22,
        class: 'USS'
    }
]

for(const person of persons) {
    for(const property in person) {
        console.log(person[`${property}`])
    }
}


