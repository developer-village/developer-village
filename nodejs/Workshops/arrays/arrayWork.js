const persons = [
    {
        firstName: 'John',
        lastName: 'Smith',
        age: 21,
        gender: 'male'
    },
    {
        firstName: 'Manoh',
        lastName: 'Sarah',
        age: 20,
        gender: 'female'
    },
    {
        firstName: 'Marc',
        lastName: 'Angelo',
        age: 17,
        gender: 'male'
    },
    {
        firstName: 'Halsey',
        lastName: 'SkinOfBlackPikin',
        age: 22,
        gender: 'male'
    }
];

for(const person of persons) {
    switch(person.age >= 21 && person.gender == 'male'){
        case true:
            console.log(`${person.firstName}` + " can play in the basketball team.")
        break;
        default:
            console.log('Sorry ' + `${person.firstName}` + ' you are not eligible to play in the basketball team.')
    }       
}
