
const fs = require('fs');
const http = require('http');

http.createServer(function(req, res) {
    fs.readFile("./index.html", (err, data) => {
        if (err){
            throw err;
        }
        else res.write(data);
        res.end();
    })
}).listen(9090);