const http = require('http');
const tip = require('./calculateTip');
const amount = require('./calculateBill');
const spent = 1250;
const tipRate = 5;

const tipValue = tip.calculateTip(tipRate, spent);

http.createServer(function(req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(`<p>The amount spent is: ${spent}</p>`);
    res.write(`<p>The tip rate is: ${tipRate}%</p>`);
    res.write(`<p>The tip is: ${tipValue}</p>`);
    res.write(`<p>The total amount is: ${amount.calculateBill(spent, tipValue)}</p>`);
    res.end();
}).listen(8000);