exports.calculateTip = calculateTip;

function calculateTip(tipRate, amount) {
    return (tipRate / 100) * amount;
}