const fs = require('fs');

const content = '\nThe cow jumped over the moon';
const filePath = 'test.txt';

const readFile = (filePath, encoding) => {
    fs.readFile(filePath, encoding, (err, data) =>{
        if(err) logError(err)
        logError(`\n'${data}' was read.`) 
    });
}

function appendFile(filePath, content){
    fs.appendFile(filePath, content, (err) => {
        if(err) logError('could not append file')
        logError(`\nthe file ${filePath} was successfully appended with ${content}`);
    })
}

function openFile(filePath, flag, fileSystem){
    fileSystem.open(filePath, flag ,(err)=>{
        if(err) logError(err) 
        logError(`\nthe file ${filePath} was successfully opened`)
    })
}

function writeFile(filePath, content, fileSystem){
    fileSystem.writeFile(filePath, content, (err)=>{
        if(err) logError(err) 
        logError(`\nthe file ${content} was successfully written`);
    })
}

function deleteFile(filePath, fileSystem){
    fileSystem.unlink(filePath, (err)=>{
        if(err) logError(err) 
        logError(`\nthe file ${filePath} was successfully deleted.`);
    })
}


function renameFile(filePath, newFilePath, fileSystem){
    fileSystem.rename(filePath, newFilePath, (err)=>{
        if(err) logError(err) 
        logError(`\nthe file ${filePath} was successfully renamed as ${newFilePath}`);
    })
}


function logError(content){
    fs.appendFile('./log.txt', content,(err)=>{
      if(err) throw err;
      console.log('')
    })
}

deleteFile('./deleteMe2.txt', fs)
// readFile(filePath, 'utf-8');