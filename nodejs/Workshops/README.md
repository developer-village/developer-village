# Creating a simple tip calculator. Saturday 15/10/2022.
*create a simple tip calculator. 
*The program should prompt for a bill amount and a tip rate. 
*The program should compute the tip and the total amount of the bill.
*The program should properly use node modules.

## Identify the inputs, the processes and the outputs.
### Inputs.
* Using the form for getting information from the user.
* The bill amount.
* The tip rate.(Percentage.)

### Processing.
* Calcuting the tip amount.
* Calculating the total amount of the bill.(tip + bill amount.)

### Output.
* Display bill amount.
* The tip amount.
* The total.(tip + bill amount.)