function getParameters(req, res){
    const parameters = {}
    const query = window.location.search
    querySubstring = query.substring(1)
    queryArray = querySubstring.split('&')
    for(const q of queryArray){
        const keyValue = q.split('=')
        parameters[keyValue[0]] = decodeURIComponent(keyValue[1])
    }
    return parameters
}

function displayTable(params){
    const formContainer = document.getElementById('formContainer')
    const table = document.createElement('table')
    const th = table.createTHead()
    const tr = th.insertRow(0)
    const key = tr.insertCell(0)
    const value = tr.insertCell(1)
    key.innerHTML = '<b>Key</b>'
    value.innerHTML = '<b>Value</b>'
    const body = table.createTBody()
    
    const paramObject = Object.entries(params)
    console.log(paramObject)
    for(let x = 0; x < paramObject.length; x++){
        const row = body.insertRow(x)
        row.insertCell(0).innerHTML = paramObject[x][0]
        row.insertCell(1).innerHTML = paramObject[x][1]
    }
    formContainer.append(table)
}