/*
 saturday 14/01/2023
 create the following table.
 student: student_id, student_matricule, student_gender, student_name, dob, address, contact
 teacher: teacher_id, teacher_name, gender, grade, salary, adress, teacher_contact
 subjects: subject_id, subject_title, coefficient, subject_description
 grade: grade_id, teacher_id, student_id, subject_id, grade, created_date
 */
START TRANSACTION;

DROP DATABASE IF EXISTS Class;

CREATE DATABASE IF NOT EXISTS Class;
CHARACTER SET utf8mb4
COLLATE utf8mb4_unicode_520_ci;

USE Class;

CREATE TABLE student(
    student_id int(04) NOT NULL AUTO_INCREMENT,
    student_matricule varchar(20) NOT NULL,
    student_gender varchar(20),
    student_name varchar(50),
    dob date,
    address varchar(50),
    contact varchar(20),
    PRIMARY KEY(student_id),
    UNIQUE(student_matricule)
);

CREATE TABLE teacher(
    teacher_id int(04) NOT NULL AUTO_INCREMENT,
    teacher_name varchar(50),
    gender varchar(20),
    grade char(01),
    salary int(8),
    address varchar(50),
    teacher_contact varchar(20),
    PRIMARY KEY(teacher_id)
);

CREATE TABLE subjects(
    subject_id int(04) NOT NULL AUTO_INCREMENT,
    subject_title varchar(30),
    coefficient int(02),
    subject_description varchar(500),
    PRIMARY KEY(subject_id)
);

CREATE TABLE grade(
    grade_id int(04) NOT NULL AUTO_INCREMENT,
    teacher_id int NOT NULL,
    student_id int NOT NULL,
    subject_id int NOT NULL,
    grade int(03) NOT NULL,
    created_date date NOT NULL,
    PRIMARY KEY(grade_id),
    CONSTRAINT FK_teacher FOREIGN KEY (teacher_id) REFERENCES teacher(teacher_id),
    CONSTRAINT FK_student FOREIGN KEY (student_id) REFERENCES student(student_id),
    CONSTRAINT FK_subjects FOREIGN KEY (subject_id) REFERENCES subjects(subject_id),
    CHECK (grade >= 0 AND grade <= 100)
);

COMMIT;

INSERT INTO `teacher` (`teacher_name`,`gender`,`grade`,`salary`,`address`,`teacher_contact`)
VALUES
  ("Theodore Hess","male","A",177919,"740-9065 Sem St.","1-685-139-5483"),
  ("Amethyst Shepherd","male","B",196817,"887-4008 Orci. St.","(743) 474-6746"),
  ("Hop Gardner","female","E",101746,"Ap #259-5495 Risus. Rd.","1-784-475-5413"),
  ("Quentin Black","male","A",104391,"139 Non St.","1-318-935-1776"),
  ("Richard Holmes","female","B",148220,"6871 Magna. St.","(511) 799-0476"),
  ("Jesse Sweet","female","A",184360,"325-1650 Et, St.","(742) 680-5164"),
  ("Xander Walton","male","D",143681,"299-9962 Adipiscing, Road","(247) 263-0850"),
  ("Skyler Preston","female","B",144409,"Ap #158-7685 Eu St.","1-552-865-6448"),
  ("Lynn Combs","male","D",109307,"Ap #478-6287 A, Avenue","1-536-643-7887"),
  ("Paki Bowers","female","C",161696,"Ap #974-2782 Placerat St.","1-929-270-4835"),
  ("Curran Kramer","female","E",115746,"Ap #375-5979 Augue Ave","(628) 185-0318"),
  ("Edan Benson","male","C",114772,"Ap #627-5912 Magna St.","1-361-778-7752"),
  ("Hu Melton","male","D",134204,"Ap #716-5254 Ullamcorper Street","1-597-567-2754"),
  ("Shelby Wilkins","female","A",154941,"Ap #299-4675 At, Street","(711) 114-4775"),
  ("Maia Warren","female","D",185272,"Ap #119-7108 Sit Ave","(755) 594-5862"),
  ("Katelyn Trujillo","female","C",146245,"8399 Malesuada Rd.","(453) 671-8447"),
  ("Ivan George","male","E",133134,"435-594 Sed Rd.","(411) 584-8149"),
  ("Edan Strong","male","C",189064,"8883 Ornare St.","(794) 634-8960"),
  ("Aspen Wilkerson","female","B",103572,"3943 Aenean Av.","(872) 641-2520"),
  ("Henry Vasquez","female","E",109394,"Ap #493-2510 Consectetuer Road","(584) 677-6511");

INSERT INTO `subjects` (`subject_title`,`coefficient`,`subject_description`)
VALUES
  ("Biology",2,"gravida nunc sed pede."),
  ("Chemistry",3,"tincidunt tempus risus. Donec egestas. Duis ac arcu. Nunc mauris. Morbi non sapien molestie orci tincidunt adipiscing. Mauris molestie pharetra nibh. Aliquam ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo eu arcu. Morbi sit amet massa. Quisque porttitor eros nec tellus. Nunc"),
  ("Mathematics",5,"commodo tincidunt nibh. Phasellus nulla. Integer vulputate, risus a ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede et risus. Quisque libero"),
  ("Physics",1,"ornare, lectus ante dictum mi, ac mattis velit justo nec ante. Maecenas mi felis, adipiscing fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin commodo ipsum. Suspendisse non leo. Vivamus nibh dolor, nonummy ac, feugiat non, lobortis quis, pede. Suspendisse dui. Fusce diam"),
  ("Food Science",10,"gravida nunc sed pede."),
  ("Law",9,"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam laoreet, libero et tristique pellentesque, tellus sem mollis dui, in sodales elit erat vitae risus. Duis a mi fringilla mi lacinia mattis. Integer eu lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit elit fermentum risus,"),
  ("Commerce",2,"laoreet ipsum. Curabitur consequat, lectus sit amet luctus vulputate, nisi sem semper erat, in consectetuer ipsum nunc id enim. Curabitur massa. Vestibulum accumsan neque et nunc. Quisque ornare"),
  ("ICT",1,"sit amet massa. Quisque porttitor"),
  ("Entrepreneurship",14,"at, iaculis quis, pede. Praesent eu dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean eget magna. Suspendisse tristique neque venenatis lacus. Etiam bibendum fermentum metus. Aenean sed pede nec ante blandit viverra. Donec tempus, lorem"),
  ("Database",7,"eu elit. Nulla facilisi. Sed neque. Sed eget lacus. Mauris non dui nec urna suscipit nonummy. Fusce fermentum fermentum arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae Phasellus ornare. Fusce"),
  ("French",10,"Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac"),
  ("English",2,"ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet,"),
  ("Geology",10,"ipsum primis in faucibus orci luctus et"),
  ("History",1,"Pellentesque ultricies dignissim lacus."),
  ("Phylosophy",2,"parturient montes, nascetur ridiculus mus. Donec dignissim magna a tortor. Nunc commodo auctor velit. Aliquam nisl. Nulla eu neque pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec est. Nunc ullamcorper, velit in aliquet lobortis, nisi nibh lacinia orci, consectetuer euismod est arcu ac orci. Ut semper pretium neque. Morbi"),
  ("Religion",6,"feugiat nec, diam. Duis mi enim, condimentum eget, volutpat ornare, facilisis eget, ipsum. Donec sollicitudin adipiscing ligula. Aenean gravida nunc sed pede. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin vel arcu eu odio tristique"),
  ("HCI",4,"sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci."),
  ("Windows server",9,"penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin vel nisl."),
  ("System administration",7,"leo elementum sem, vitae aliquam eros turpis non enim. Mauris quis turpis vitae purus gravida sagittis. Duis gravida. Praesent eu nulla at sem molestie sodales. Mauris blandit enim consequat purus. Maecenas libero est, congue a, aliquet vel, vulputate eu, odio."),
  ("Enterprise Organization",1,"quis arcu vel quam dignissim pharetra. Nam ac nulla. In tincidunt congue turpis. In condimentum. Donec at");

INSERT INTO `student` (`student_matricule`,`student_gender`,`student_name`,`dob`,`address`,`contact`)
VALUES
  ("DCB29HNF4LT","male","Jamal Sawyer","2023-10-14 20:05:24","364-781 Quis, Ave","(442) 580-4979"),
  ("EGG65DFV3BO","male","Stewart Hayes","2023-06-15 14:54:14","Ap #453-2078 Neque Rd.","(153) 587-6513"),
  ("NSQ84UWW6SU","female","Ainsley Waters","2023-12-13 20:25:56","Ap #567-6053 At Avenue","1-813-290-4954"),
  ("RYU48PUM4IN","male","John Camacho","2023-02-07 02:18:32","136-5442 Scelerisque Rd.","(551) 850-8642"),
  ("AUM61QKQ2XS","male","Cullen Fisher","2024-01-05 19:39:07","Ap #568-2233 Erat, St.","1-272-378-5855"),
  ("TMG38TUG1YG","female","Nash Owen","2023-01-05 03:49:48","P.O. Box 507, 4434 Adipiscing. Road","1-255-884-5580"),
  ("JVN77ETZ3FY","male","Francesca Farrell","2022-02-13 00:50:29","5902 Molestie Ave","(841) 475-7677"),
  ("QXR95XXK3EY","male","Whoopi Dominguez","2022-12-24 23:55:18","782-8179 Arcu St.","(837) 431-2273"),
  ("DHE22FEV9DK","male","Jerry Pennington","2022-03-14 17:11:16","1964 Tempus, Avenue","1-560-375-4266"),
  ("RNM54EKV8VN","male","Merrill Small","2022-09-09 06:48:15","Ap #365-1991 Nisi. St.","1-564-167-5321"),
  ("YGG80DSJ7LM","male","Thane Russell","2023-08-27 22:10:21","808-9153 Condimentum Street","(551) 332-3685"),
  ("JZP37NON5SH","female","Cameron Wagner","2023-09-27 16:29:31","Ap #895-8928 Donec Street","(647) 407-4397"),
  ("IPS42KUY3LL","male","Kevyn Warren","2023-03-08 17:13:14","Ap #195-4101 Tincidunt St.","1-296-818-7642"),
  ("OHO94JTI0VS","female","Jack Osborne","2023-02-24 10:59:04","Ap #684-2500 Ante St.","1-884-713-4740"),
  ("QTC77EHP7AP","female","Dolan Weeks","2022-11-16 21:52:12","893-8434 Luctus. Avenue","1-772-361-2225"),
  ("GIW24YRB7QP","female","Juliet Warner","2023-03-23 04:36:52","Ap #346-7094 Montes, St.","(571) 752-6579"),
  ("RMP84PYE4VK","female","Hope Rios","2022-04-24 02:50:04","380-3634 Vitae, Ave","1-861-865-6067"),
  ("CXF24CVK3BU","male","Nero Sears","2022-12-03 15:54:27","Ap #258-584 Interdum St.","1-191-921-5696"),
  ("XKI47WXF3IS","female","Yeo Burgess","2022-02-26 01:29:24","779-6755 A Ave","1-818-355-0248"),
  ("HLK58HHY2CT","male","Jonah Yates","2023-12-08 21:41:01","P.O. Box 658, 6560 A, Road","1-276-963-5524");


INSERT INTO `grade` (`teacher_id`,`student_id`,`subject_id`,`grade`,`created_date`)
VALUES
  (8,15,5,37,"2022-10-08 01:26:04"),
  (19,14,17,67,"2023-03-01 11:21:46"),
  (15,2,4,33,"2022-05-11 10:02:00"),
  (1,11,6,74,"2023-03-18 11:41:18"),
  (13,3,3,16,"2022-09-03 02:41:01"),
  (2,2,14,76,"2023-07-09 04:08:53"),
  (4,17,10,26,"2023-02-18 18:41:06"),
  (8,20,11,37,"2022-07-25 18:47:28"),
  (15,8,3,25,"2023-08-28 22:00:07"),
  (6,19,16,64,"2023-09-23 07:30:19"),
  (13,11,5,77,"2023-08-07 22:21:09"),
  (12,7,13,1,"2023-07-20 22:34:36"),
  (4,11,16,79,"2023-08-05 21:15:17"),
  (18,14,9,78,"2023-02-23 16:38:02"),
  (1,10,12,57,"2022-06-03 10:52:52"),
  (10,13,11,52,"2023-02-05 13:00:15"),
  (9,15,8,53,"2023-12-30 12:09:01"),
  (6,11,16,53,"2023-02-25 06:38:32"),
  (4,4,15,2,"2022-02-13 10:21:24"),
  (5,10,8,42,"2023-02-27 21:47:30");