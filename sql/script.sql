-- getting the customer with the highest number of orders.
SELECT
    C.CustomerID,
    C.CustomerName,
    COUNT(Orders.OrderID) AS "Total orders"
FROM
    Customers C
    LEFT JOIN Orders USING(CustomerID)
GROUP BY
    (C.CustomerID)
ORDER BY
    "Total orders" DESC
LIMIT
    1;



SELECT C.CustomerName, SUM(Price*Quantity) AS "total_amount" 
FROM Orders O
INNER JOIN Customers C
USING(CustomerID)
INNER JOIN OrderDetails
USING(OrderID)
INNER JOIN Products
USING(ProductID)
GROUP BY CustomerName)
ORDER BY total_amount
LIMIT 1;



-- Describe the properties of a database called "class".
DESCRIBE Class;

-- using the database called "manorDB"
use manorDB;

-- modifying the contents of a table with the ALTER command.
ALTER TABLE subjects
MODIFY COLUMN subject_description varchar(500) NOT NULL;


-- deleting the content of the table student without deleting the table itself.
TRUNCATE TABLE student;


-- Decribing the contents of a table with the DESC 0R EXPLAIN keywords.
desc students;
explain students;