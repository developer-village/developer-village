namespace BankingApplication.Banking;

public class BankingUser : IBankingUser
{
    public string FirstName { get; private set; }
    public string Name { get; private set; }

    public string LastName { get; private set; }

    public DateOnly DoB { get; private set; }

    public Gender UserGender { get; private set; }

    public string Nationality { get; private set; }
    public BankingUser( string firstName, string lastName, DateOnly dob, Gender gender, string nationality ){
        FirstName = firstName; 
        LastName = lastName;
        DoB = dob;
        UserGender = gender;
        Nationality = nationality;
        Name = firstName + " " + lastName;
    }
}