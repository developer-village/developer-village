namespace BankingApplication.Banking;

public class BankingAccount : IBankingAccount
{
    public decimal Balance { get;  set; }

    public IBankingUser Owner { get;  private set; }

    public string AccountNumber { get;  set; }
    public DateTime CreatedAt { get;  private set; }
    public BankingAccount( decimal balance, IBankingUser owner, string accountNumber, DateTime createdAt ){
        Balance = balance;
        Owner = owner;
        AccountNumber = accountNumber;
        CreatedAt = createdAt;
   }

}