namespace BankingApplication.Banking;

public class BankingCredentials : IBankingCredentials
{
    public int Pin { get; set; }

    public IBankingAccount BankAccount { get; private set; }
    public BankingCredentials( int pin, IBankingAccount bankAccount){
       Pin = pin;
       BankAccount = bankAccount; 
    }

}
