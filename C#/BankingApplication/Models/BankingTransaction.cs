namespace BankingApplication.Banking;

public class BankingTransaction : IBankingTransactions
{
    IBankingCredentials BankingCredentials;
    public BankingTransaction(IBankingCredentials bankingCredentials){
        BankingCredentials = bankingCredentials;
    }
    public decimal CheckBalance()
    {
        return BankingCredentials.BankAccount.Balance;
    }

    public bool Deposit(decimal amount, string depositor, string? message)
    {
        BankingCredentials.BankAccount.Balance += amount;
        Console.WriteLine ($"Depositor: {depositor} Message: {message} New Balance: {BankingCredentials.BankAccount.Balance} Transaction Time: {DateTime.Now}");
        return true;
    }


    public bool Withdrawal(decimal amount, string? message)
    {
        if (amount <= BankingCredentials.BankAccount.Balance){
          BankingCredentials.BankAccount.Balance -= amount; 
          Console.WriteLine ($"Message: {message} New Balance: {BankingCredentials.BankAccount.Balance} Transaction Time: {DateTime.Now}");
          return true;
        } else {
            Console.WriteLine ("Transaction failed due to insufficient funds.");
            return false;
        }
    }
}
