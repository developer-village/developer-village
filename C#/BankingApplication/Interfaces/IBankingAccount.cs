namespace BankingApplication.Banking;
public interface IBankingAccount{
    decimal Balance { get; set; }
    IBankingUser Owner{ get; }
    string AccountNumber { get; set; }
    DateTime CreatedAt { get; }
}