namespace BankingApplication.Banking;
public interface IBankingCredentials{
    int Pin { get; set; }
    IBankingAccount BankAccount { get; }
    }   