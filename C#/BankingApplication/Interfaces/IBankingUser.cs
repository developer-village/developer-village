namespace BankingApplication.Banking;
public interface IBankingUser{
    string FirstName{ get; }
    string LastName{ get; }
    DateOnly DoB{ get; }
    Gender UserGender{ get; }
    string Nationality { get; }
}