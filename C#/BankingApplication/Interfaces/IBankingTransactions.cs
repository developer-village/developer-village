namespace BankingApplication.Banking;
public interface IBankingTransactions{
    bool Withdrawal( decimal amount, string? message );
    bool Deposit( decimal amount, string depositor, string? message );
    decimal CheckBalance();
}