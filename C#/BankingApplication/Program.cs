﻿using MySqlConnector;
// set these values correctly for your database server
var builder = new MySqlConnectionStringBuilder
{
	Server = "localhost",
	UserID = "root",
	Password = "PHARAONEXE",
	Database = "sakila",
};

// open a connection asynchronously
using var connection = new MySqlConnection(builder.ConnectionString);
await connection.OpenAsync();

// create a DB command and set the SQL statement with parameters
using var command = connection.CreateCommand();
command.CommandText = @"SELECT * FROM actor WHERE actor_id = @actor_id;";
command.Parameters.AddWithValue("@actor_id", 1);

// execute the command and read the results
using var reader = await command.ExecuteReaderAsync();
while (reader.Read())
{
	var id = reader.GetInt32("actor_id");
    var first_name = reader.GetString("first_name");
    var last_name = reader.GetString("last_name");
    var last_update = reader.GetDateTime("last_update");
	
	// ...
	Console.WriteLine($"{id}, {first_name}, {last_name}, {last_update}");
}