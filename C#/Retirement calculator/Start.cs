namespace RetirementCalculator.Start
{
    public class Start
    {
        public string name;
        public DateTime dateOfBirth;
        public int userAge;
        public int pensionAge;
        public int retirementAge;
        public int retirementDate;
        public string UserAnswer{
            get; set;
        }
        public bool AnswerStatus{
            get; set;
        }

        public Start(){
            AnswerStatus = true;
            UserAnswer = AnswerStatus ? "yes" : "no";
        }
        public string? GetUserName()
        {
            Console.WriteLine("Hello what is your surname?");
            string? x = Console.ReadLine();
            return name = x;
        }
        public int GetUserAge()
        {
            beginning:
            Console.WriteLine($"What is your date of birth? yyyy-mm-dd");
            string dateAsString = Console.ReadLine();
            DateTime y = Convert.ToDateTime(dateAsString);
            dateOfBirth = y;
            int yearOfBirth = dateOfBirth.Year;
            int z = 2022 - yearOfBirth;
            Console.WriteLine($"Your age is: {z}, is that right? answer by yes or no.");
            UserAnswer = Console.ReadLine();
            if(UserAnswer == "yes"){
                return userAge = z;
            }
            else{
                goto beginning;
            }
        }
        public int CalculatePensionAge()
        {
            return pensionAge = 70 - userAge;
        }
        public int CalculateRetirementDate(){
            return retirementDate = 2022 + pensionAge;
        }
        public void DisplayMessage()
        {
            Console.WriteLine($"Hello {name}, your age is {userAge}.");
            if(pensionAge > 0){
                Console.WriteLine($"You have {pensionAge} years till retirement." +
                $"You will be retiring in the year:{retirementDate}.");
            }
            else{
                Console.WriteLine("Congratulations! You have arrived at your retirement age." +
                "Enjoy your duely earned pension.");
            }
        }
    }
}