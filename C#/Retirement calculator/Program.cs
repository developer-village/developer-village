﻿using RetirementCalculator.Start;

Start start = new Start();
start.GetUserName();
start.GetUserAge();
start.CalculatePensionAge();
start.CalculateRetirementDate();
start.DisplayMessage();