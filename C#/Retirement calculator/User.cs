namespace RetirementCalculator.User
{
    using RetirementCalculator.Start;
    public class User : Start
    {
        public User(string name, int age)
        {
            Name = name;
            Age = age;
        }
        public string Name
        {
            get { return Name; }
            set { Name = value; }
        }
        public int Age
        {
            get { return Age; }
            set { Age = value; }
        }
    }
}