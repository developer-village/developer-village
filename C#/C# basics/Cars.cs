namespace CarsNamespace
{
    class Cars
    {
        public Cars()
        {
            Brand = "Toyota";
            Color = "red";
            Model = "SUV";
            IsTaxed = false;
            TaxString = IsTaxed ? "taxed" : "not taxed";
            Reg = "abc123";
        }
        public string Brand
        {
            get; set;
        }
        public string Color
        {
            get; set;
        }
        public string Model
        {
            get; set;
        }
        public bool IsTaxed
        {
            get; set;
        }
        public string Reg
        {
            get; set;
        }
        public string TaxString
        {
            get; set;
        }
        public void DisplayProperties()
        {
            Console.WriteLine($"The car brand is {Brand}, the color is {Color}," +
            $@"the model is {Model}, the registration number is {Reg}, and the car is {TaxString}");
        }
    }
}