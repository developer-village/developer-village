namespace Foodnamespace{
    public class Food{
        public string Color{
            get; private set;
        }
        public Food(string color = "black"){
            Color = color;
        }
        public void DisplayColorInfo(){
            Console.WriteLine(Color);
        }
    }
    public class Banana : Food{

    }   
}

