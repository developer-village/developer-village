namespace PersonNamespace
{
    public class Person
    {
        public void DisplayName()
        {
            Console.WriteLine("steve nya");
        }
        public static int DisplayAge()
        {
            return 50;
        }
        public static void DisplayMessage(string name = "steve nya", int age = 50){
            Console.WriteLine($"Hello {name}, you are {age} years old.");
        }
        public void Display(){
            Console.WriteLine("This is the Display() method.");
        }
        public void Display(string name){
            Console.WriteLine($"Hello {name}, how are you today.");
        }
        public void Display(int age){
            Console.WriteLine($" you are {age} years old.");
        }
    }
}
