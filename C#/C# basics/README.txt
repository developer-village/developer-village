                 How to code in C# language using VScode.
                 ---------------------------------------
-Download and install dotnet coding pack.
-To check if it's installed, run "dotnet --version" in your terminal. If success, 
 then you will get the version displayed to the console.
-In your extensions, install .NET Extension pack, .NET Install Tool for Extension Authors,
 .NET Education Bundle SDK install tool.
-Create a folder for your project.
-In the terminal move to the directory of your C# project and execute the 
 command "dotnet new console" to initialize it.
-To execute your C# script, run the command "dotnet run" in the terminal.