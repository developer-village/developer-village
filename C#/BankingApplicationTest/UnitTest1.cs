namespace BankingApplicationTest;

public class UnitTest1
{
    [Fact]
    public void BankingUser_ConstructorSetProperty_Successful()
    {
        // Arrange.
        string firstName = "John";
        string lastName = "Doe";
        DateOnly dob = new DateOnly(2000,10,10);
        Gender userGender = Gender.Male;
        string nationality = "Cameroonian";

        // Act.
        BankingUser sut = new BankingUser(firstName,lastName,dob,userGender,nationality);

        // Assert.
        Assert.Equal(firstName, sut.FirstName);
        Assert.Equal(lastName, sut.LastName);
        Assert.Equal(dob, sut.DoB);
        Assert.Equal(userGender, sut.UserGender);
        Assert.Equal(nationality, sut.Nationality);
    }
}